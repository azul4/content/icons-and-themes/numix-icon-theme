# numix-icon-theme

Numix is the official icon theme from the Numix Project

https://github.com/numixproject/numix-icon-theme

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/numix-icon-theme.git
```
